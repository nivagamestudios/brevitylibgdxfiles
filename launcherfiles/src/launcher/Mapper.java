package launcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Mapper {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		System.out.println("Please enter the directory");
		String dir = scn.nextLine();
		scn.close();

		try{
			ArrayList<File> allPreselectFiles = new ArrayList<File>();
			ArrayList<File> allFiles = new ArrayList<File>();
			
			File start = new File(dir);

			List<File> files = FileHandler.getFileListing(start);
			for(File file : files){
				allPreselectFiles.add(file);
			}

			for(File file: allPreselectFiles){
				if(file.isFile() && FileHandler.extension(file).equals("jar")){
					allFiles.add(file);
				}
			}
			
			StringBuffer buffer = new StringBuffer();
			
			for(File f : allFiles){
				
				String path = f.getAbsolutePath();
				String base = dir;
				String relative = new File(base).toURI().relativize(new File(path).toURI()).getPath();
				
				String fileName = relative;
				String md5 = getMD5(f);
				
				buffer.append(fileName + "`" + md5 + "\r\n");
				
			}

			System.out.println(buffer.toString());
			
			FileOutputStream fos = new FileOutputStream(new File(dir + "/index.txt"));
			fos.write(buffer.toString().getBytes());
			fos.close();
			
			File f = new File(dir + "/executables");
			File[] allExe = f.listFiles();
			
			for(int i = 0; i < allExe.length / 2; i++)
			{
			    File temp = allExe[i];
			    allExe[i] = allExe[allExe.length - i - 1];
			    allExe[allExe.length - i - 1] = temp;
			}
			
			
//			ArrayList<File> sorted = new ArrayList<File>();
//			for(File loopFile:allExe){
//				boolean done = false;
//				while(!done){
					
//				}
				
				
//			}
			
			
			StringBuffer sb = new StringBuffer();
			for(File file:allExe){
				sb.append(file.getName().split("\\." + FileHandler.extension(file))[0] + "`" + "\r\n");
			}
			
			FileOutputStream fos2 = new FileOutputStream(new File(dir + "/versions.txt"));
			fos2.write(sb.toString().getBytes());
			fos2.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}


	}

	public static String getMD5(File f) {

		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			FileInputStream fis = new FileInputStream(f);

			byte[] dataBytes = new byte[1024];

			int nread = 0;
			while((nread = fis.read(dataBytes)) != -1){
				md.update(dataBytes, 0, nread);
			}
			;
			byte[] mdbytes = md.digest();

			//convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for(int i = 0; i < mdbytes.length; i++){
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			fis.close();
			return sb.toString();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}

	}
}
