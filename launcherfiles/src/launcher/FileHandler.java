package launcher;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileHandler {

	public static List<File> getFileListing(File aStartingDir) throws FileNotFoundException {
		validateDirectory(aStartingDir);
		List<File> result = getFileListingNoSort(aStartingDir);
		Collections.sort(result);
		return result;
	}

	// PRIVATE
	private static List<File> getFileListingNoSort(File aStartingDir) throws FileNotFoundException {
		List<File> result = new ArrayList<>();
		File[] filesAndDirs = aStartingDir.listFiles();
		List<File> filesDirs = Arrays.asList(filesAndDirs);
		for(File file : filesDirs){
			result.add(file); //always add, even if directory
			if(!file.isFile()){
				//must be a directory
				//recursive call!
				List<File> deeperList = getFileListingNoSort(file);
				result.addAll(deeperList);
			}
		}
		return result;
	}
	
	private static void validateDirectory(File aDirectory) throws FileNotFoundException {
		if(aDirectory == null){
			throw new IllegalArgumentException("Directory should not be null.");
		}
		if(!aDirectory.exists()){
			throw new FileNotFoundException("Directory does not exist: " + aDirectory);
		}
		if(!aDirectory.isDirectory()){
			throw new IllegalArgumentException("Is not a directory: " + aDirectory);
		}
		if(!aDirectory.canRead()){
			throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
		}
	}
	

	public static String extension(File f){
		String extension = "";

		int i = f.getName().lastIndexOf('.');
		int p = Math.max(f.getName().lastIndexOf('/'), f.getName().lastIndexOf('\\'));

		if (i > p) {
		    extension = f.getName().substring(i+1);
		}
		
		return extension;
	}
	
}
