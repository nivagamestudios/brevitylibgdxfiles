package gaoyoland.gitloc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	static Runtime rt = Runtime.getRuntime();

	static ArrayList<String> users = new ArrayList<String>();
	static ArrayList<Integer> changed = new ArrayList<Integer>();
	static ArrayList<Integer> commits = new ArrayList<Integer>();

	static ArrayList<Integer> deletes = new ArrayList<Integer>();
	static ArrayList<Integer> inserts = new ArrayList<Integer>();

	static ArrayList<Integer> remains = new ArrayList<Integer>(); //Only works for 2 person teams

	static int lastUser = 0;

	public static void main(String[] args) throws IOException {

		Scanner scn = new Scanner(System.in);
		System.out.println("Enter the directory");
		ArrayList<String> output = runCommand(scn.nextLine());
		scn.close();

		for(String x : output){

			if(x.startsWith("Author:")){ //Author Message				
				String user = x.split(":")[1].split("<")[0].trim();

				if(users.contains(user)){
					lastUser = users.indexOf(user);
					commits.set(lastUser, commits.get(lastUser) + 1);
				}else{
					users.add(user);
					changed.add(0);
					commits.add(1);
					inserts.add(0);
					deletes.add(0);
					lastUser = users.indexOf(user);
				}

			}

			if(x.endsWith("(-)") || x.endsWith("(+)")){ // Change Message
				String[] comma = x.split(",");

				if(comma.length == 3){
					int insert = Integer.parseInt(comma[1].split("insertions")[0].split("\\s+")[1]);
					int delete = Integer.parseInt(comma[2].split("deletions")[0].split("\\s+")[1]);

					changed.set(lastUser, changed.get(lastUser) + insert + delete);
					inserts.set(lastUser, inserts.get(lastUser) + insert);
					deletes.set(lastUser, deletes.get(lastUser) + delete);
				}else if(comma.length == 2){
					if(comma[1].split("deletions").length == 2){
						int insert = Integer.parseInt(comma[1].split("insertions")[0].split("\\s+")[1]);

						changed.set(lastUser, changed.get(lastUser) + insert);
						inserts.set(lastUser, inserts.get(lastUser) + insert);
					}else if(comma[1].split("insertions").length == 2){
						int delete = Integer.parseInt(comma[1].split("deletions")[0].split("\\s+")[1]);
						changed.set(lastUser, changed.get(lastUser) + delete);
						deletes.set(lastUser, deletes.get(lastUser) + delete);
					}

				}

			}
		}

		ArrayList<String> arguments = new ArrayList<String>();
		
		for(String s:args){
			arguments.add(s);
		}
		
		if(arguments.contains("--brevity")){

			int index = users.indexOf("FireRaven101");
			changed.set(index, changed.get(index) - 2164);

			int indexF = users.indexOf("freakshow1217");
			changed.set(indexF, changed.get(indexF) + 721 - 1480 + 446);

		}
		

		int totalInsertions = 0;
		for(int i : inserts){
			totalInsertions += i;
		}

		int totalDeletions = 0;
		for(int i : deletes){
			totalDeletions += i;
		}

		System.out.println();

		System.out.println("Repo Total Stats: ");
		System.out.println("Total Insertions: " + totalInsertions);
		System.out.println("Total Deletions: " + totalDeletions);
		
		System.out.println();
		
		for(int i = 0; i < users.size(); i++){
			int userChanged = inserts.get(i) + deletes.get(i);
			int userCommits = commits.get(i);
			int userInserts = inserts.get(i);
			int userDeletes = deletes.get(i);
			double userCommitRatio = round((double) userChanged / (double) userCommits, 2);
			double insertDeleteRatio = round((double) userInserts / (double) userDeletes, 2);
			String insertionPercentage = " (" + round(((double) userInserts / (double) totalInsertions) * 100, 2) + "%)";
			String deletionPercentage = " (" + round(((double) userDeletes / (double) totalDeletions) * 100, 2) + "%)";
			
			System.out.println("User: " + users.get(i));
			System.out.println("Changed: " + userChanged);
			System.out.println("Commits: " + userCommits);
			if(!arguments.contains("--short")){
				System.out.println("Changes to Commit Ratio: " + userCommitRatio + " lines per commit");
				System.out.println("Insertions: " + userInserts + insertionPercentage);
				System.out.println("Deletions: " + userDeletes + deletionPercentage);
				System.out.println("Insert to Delete Ratio: " + insertDeleteRatio + " insertions per deletion");
			}
			System.out.println();
		}

	}

	public static ArrayList<String> runCommand(String filePath) {

		try{
			ArrayList<String> output = new ArrayList<String>();

			Process p = rt.exec("cmd /C cd " + filePath + " && " + "cmd /C git log --reverse --shortstat");

			Scanner scn = new Scanner(p.getInputStream());

			while(scn.hasNext()){
				output.add(scn.nextLine());
			}

			Scanner scn2 = new Scanner(p.getErrorStream());
			while(scn2.hasNext()){
				System.err.println(scn2.nextLine());
			}

			scn.close();
			scn2.close();

			return output;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public static double round(double r, double sigs) {
		if(sigs == 0){
			return Math.round(r);
		}
		
		if(sigs < 0){
			throw new IllegalArgumentException("Significant digits must be >= 0");
		}
		
		double scaleUp = Math.pow(10, sigs);
		double x = r * scaleUp;
		double y = Math.round(x) / scaleUp;
		return y;
	}

}
